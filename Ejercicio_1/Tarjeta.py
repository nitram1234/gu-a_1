#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Tarjeta():

    def __init__(self, monto, dueno):
        self.monto = monto
        self.dueno = dueno

    def get_monto(self):
        return self.monto
    def get_dueno(self):
        return self.dueno

    def set_monto(self, monto):
        self.monto = monto
    def set_dueno(self, dueno):
        self.dueno = dueno
        
