#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  main.py
#


from Cuenta import Cuenta

if __name__ == '__main__':
	
    dueno_cuenta = input("Ingrese su nombre de usuario: ")
    monto_cuenta = float(input("Ingrese su monto inicial: "))
    numero_cuenta = int(input("Ingrese su numero de cuenta: "))
	
    cuenta = Cuenta(monto_cuenta, dueno_cuenta, numero_cuenta)	
	
    cuenta.set_dueno(dueno_cuenta)
    cuenta.set_monto(monto_cuenta)
    cuenta.set_numero(numero_cuenta)
	
    print("Ingrese su contraseña (su nombre de usuario): ")
    contrasena = str(input("input: "))

    i = 0
    while True:
		
        print(" Ingrese ´D´ para depositar a su cuenta ")
        print(" Ingrese ´R´ para retirar de su cuenta ")
        print(" Ingrese ´A´ para ver datos de su cuenta ")
        print(" Ingrese ´T´ para transferir a otra cuenta ")
        print(" Ingrese ´S´ para salir ")
        
        opt = str(input("input: "))
        
        if (i == 4):
            break


        if (contrasena == dueno_cuenta):
			
            if (opt == "A"):
				
                print(" El nombre del titular es: {0}".format(cuenta.dueno))
                print(" Su numero de cuenta es: {0}".format(cuenta.numero)) 
                print(" Su saldo disponible es: {0}".format(cuenta.monto))
                
            
            elif(opt == "D"):

                deposito = int(input(" Ingrese monto a despositar a su cuenta: "))
                cuenta.deposito(deposito)
                
            
            elif (opt == "R"):

                retiro = int(input(" Ingrese monto a retirar de su cuenta: "))
                cuenta.retiro(retiro)
                
                
            elif (opt == "T"):
            
                destino = str(input(" Ingrese dueño de la cuenta destino: "))
                dinero = int(input(" Ingrese monto: "))
                cuenta.transferencia(destino, monto)
                
            elif (opt == "S"):
                break




        else:
            print("contraseña incorrecta, intentelo nuevamente: ")
            print("Le quedan 3 intentos")
            i = i+1
          
            contrasena=input()
	
	
	
